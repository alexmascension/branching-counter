
from os.path import dirname, abspath
import time
import os

import numpy as np
import pandas as pd
from scipy import sparse, ndimage


from skimage import color
from skimage import io
from skimage.morphology import medial_axis, skeletonize, skeletonize_3d, watershed, thin, closing
from skimage.util import invert
from skimage.restoration import (denoise_tv_chambolle, denoise_bilateral,
                                 denoise_wavelet, estimate_sigma)
from skimage.filters import roberts, sobel, scharr, prewitt
from skimage.exposure import equalize_hist, equalize_adapthist, histogram
from skimage.measure import label

import matplotlib.pyplot as plt

import itertools as it

from numba import jit
from line_profiler import LineProfiler

import h5py
from functools import wraps

from numba import jit



def normalize(array, percentile=95):
    array = (array - np.amin(array)) / (
            np.percentile(array.ravel(), percentile) - np.amin(array))
    array[array > 1] = 1

    return array


def tv_def(array, ntrials_max, thres):
    i = 0
    while i < ntrials_max:
        denoised_array = denoise_tv_chambolle(array, weight=0.05, multichannel=False)
        contrast_im = normalize(percentile=97, array=denoised_array)
        diff = np.sum((contrast_im - array) ** 2)
        array = contrast_im.copy()
        if diff < thres: break
        i += 1
    return array


def contrast_eqadapthist(array, ntrials_max, thres, low_pass):
    i = 0
    while i < ntrials_max:
        contrast_im = normalize(array=equalize_adapthist(array))
        diff = np.sum((contrast_im - array) ** 2)
        array = contrast_im.copy()
        if diff < thres: break
        i += 1

    array[array < low_pass] = 0

    return array


def get_skeleton(array):
    skel, distance = medial_axis(array>0, return_distance=True)
    distance_on_skel = distance * skel
    distance_on_skel = distance_on_skel / np.amax(distance_on_skel)
    return distance_on_skel


def flatten_array(array, method='sum'):
    if method.upper() == 'SUM':
        return sum([i for i in array])
    elif method.upper() == 'MEAN':
        return sum([i for i in array])/len(array)




def _take_areas(array, n_start=10):
    """
    Given an skeleton array, transforms the array into a binarized array of 10s and 0s, creates
     a frame of zeros. and uses `skimage.measure.label` method to obtain labels of the areas
     with 10s. Then returns those labels + the original array.

    Parameters
    ----------
    array: `np.ndarray`
        Array with skeleton structure.
    n_start: int
        Number from which labelled array will start.

    Returns
    -------
    `np.ndarray`
        Array with labeled areas.
    """

    canvas = np.zeros((array.shape[0] + 2, array.shape[1] + 2))
    trasharray = array.copy()
    trasharray[trasharray == 0] = 10
    trasharray[trasharray <= 1] = 0
    canvas[1:-1, 1:-1] = trasharray
    labels = label(canvas, neighbors=4)[1:-1, 1:-1]
    labels[labels > 0] += n_start
    return labels + array


def _mark_crossings(array, w=5, k=3, verbose = 0):
    """
    Given an skeletonized array, using a sliding window determines the positions where
    branching events occur.

    Parameters
    ----------
    array: `np.ndarray`
        Skeletonized array.
    w: int
        Window of analysis. For each iteration a w x w array will be analyzed to see if there
        is a branching event there.
    k: int
        Marking width. If there is a branching event, a k x k ones array will be printed within
        the array of branchings to mark where the branching event within the image occured.

    Returns
    -------
    array_positives: `np.ndarray`
        Array of the same size as the image array with 1 and 0, where 1 mean branching events.
    """

    array = array.copy()

    middle = int(w / 2) + 1

    array_positives = np.zeros((np.shape(array)[0], np.shape(array)[1]))

    x, y, = middle, middle
    len_x = array.shape[0]

    while x <= len_x - middle:
        if verbose:
            if x % 100 == 0: print(x, len_x - middle)
        while y <= np.shape(array)[1] - middle:
            subarray = _take_areas(array[x - middle:x + middle, y - middle:y + middle])
            array_borders0 = np.concatenate(
                (np.ravel(subarray[0, :]), np.ravel(subarray[:, -1]),
                 np.ravel(subarray[-1, :])[::-1], np.ravel(subarray[:, 0])[::-1]))

            array_ravel = np.ravel(subarray)
            list_areas = array_ravel[array_ravel > 1]
            list_areas = list_areas - min(list_areas)
            list_areas = list(set(list_areas))

            array_borders = array_borders0[array_borders0 > 1]
            array_borders = array_borders - min(array_borders)
            array_borders = [int(i[0]) for i in it.groupby(list(array_borders))]

            thereiscross1, thereiscross2 = False, False

            inner_areas = [i for i in list_areas if i not in array_borders]

            proportion_skel_border = 1-len(array_borders)/len(array_borders0)

            if proportion_skel_border > 0.17:
                if len(array_borders) > 1:
                    if len(set(array_borders[::2])) > 1:
                        # In this way we avoid cases like this
                        # 0 0 X 1 1 1 1 1
                        # 0 0 0 X 1 1 1 1
                        # 0 0 0 0 X 1 1 1
                        # X 0 0 0 0 X X X
                        # 3 X 0 0 0 0 0 0
                        # 3 3 x 0 0 0 0 X
                        # 3 3 3 X 0 0 X 2
                        # 3 3 3 X 0 X 2 2

                        if ((len(inner_areas) > 0) & (len(inner_areas) < 3)):
                            # We put an upper limit for cases where there are two diagonal parallel
                            # lines and many small 1-pixel areas appear
                            # We avoid cases like that
                            #  0 0 0 0 0 0 0
                            #  0 0 0 0 0 0 0
                            #  x 0 0 0 0 0 0
                            #  1 x 0 0 0 0 0
                            #  x 2 x 0 0 0 0
                            #  5 x 3 x 0 0 0
                            #  5 5 x 4 x 0 0

                            for area in inner_areas:
                                # We make sure that all inner areas are bigger than 1
                                if len(array_ravel[array_ravel == area]) > 1:
                                    thereiscross1 = True
                        elif len(inner_areas) == 0:
                            thereiscross1 = True

                        if (array_borders != array_borders[::-1]) & (subarray[0, 0] > 1) & \
                                (subarray[0, 0] > 1) & (subarray[0, 0] > 1) & (subarray[0, 0] > 1):
                            # We make sure that the sequence is not palindromic, but also
                            # make sure that the vertices don't contain skeletons to avoid
                            # cases like that
                            # x 0 0 0 0
                            # 0 x 0 0 0
                            # 0 x 0 0 0
                            # 0 x 0 0 0
                            # 0 0 x 0 0  -> [1, 2] = Not palindromic (False Positive)
                            thereiscross2 = True

            if (thereiscross1 & thereiscross2):
                array_positives[x - k:x + k, y - k:y + k] = 1

            y += middle

        x += middle
        y = middle

    return array_positives


def get_branching_points(array, w, k, verbose):
    """
    Given an skeletonized array, using a sliding window determines the positions where
    branching events occur.

    Parameters
    ----------
    array: `np.ndarray`
        Skeletonized array.
    w: int
        Window of analysis. For each iteration a w x w array will be analyzed to see if there
        is a branching event there.
    k: int
        Marking width. If there is a branching event, a k x k ones array will be printed within
        the array of branchings to mark where the branching event within the image occured.

    Returns
    -------
    array_positives: `np.ndarray`
        Array of the same size as the image array with 1 and 0, where 1 mean branching events.
    """

    array_positives_final = _mark_crossings(array, w, k, verbose)
    array_positives_sub = _mark_crossings(
        array[int(w / 2) + 1:-int(w / 2), int(w / 2) + 1:-int(w / 2)], w, k, verbose)

    array_positives_final[int(w / 2) + 1:-int(w / 2), int(w / 2) + 1:-int(w / 2)] = \
        array_positives_final[int(w / 2) + 1:-int(w / 2),
        int(w / 2) + 1:-int(w / 2)] + array_positives_sub

    array_positives_final[array_positives_final > 1] = 1

    return array_positives_final


def get_stack(array, n_slides, sliding, method):
    """
    Given a 3D array, returns another 3D array where each layer is a combination of several
    layers from the original array.

    Parameters
    ----------
    array: `np.ndarray`
        Skeletonized array.
   n_slides: int
       Number of slides (2D arrays) that will be considered at each combination iteration.
       If 0, combined all the layers into a 2D array.
   sliding: bool
       If `True`, performs the combination using a aliding window of one; e.g. if there are
       10 slides and `n_slides=3`, then [0,1,2] are stacked together, then [1,2,3], and so on
       until [8,9,10]. If `False`, then [0,1,2], [3,4,5], [6,7,8], [9] are combined.
   method: str
       [`mean`, `sum`] Averages the signal across slides or sums it.

   Returns
   -------
   labels: `np.ndarray`
       Stacked array.
   """

    def applyop(array, method):
        if method.upper() == 'SUM':
            return np.sum(array, axis=0)
        elif method.upper() == 'MEAN':
            return np.mean(array, axis=0)

    if n_slides == 0:
        return applyop(array, method)
    else:
        list_combs = []
        if sliding:
            i = 0
            while i <= len(array) - n_slides:
                list_combs.append([j for j in range(i, i + n_slides)])
                i += 1
        else:
            i = 0
            while i <= len(array) - n_slides:
                list_combs.append([j for j in range(i, max(i + n_slides, n_slides))])
                i += n_slides

        list_arrays = []

        for comb in list_combs:
            list_arrays.append(applyop(np.array([array[i] for i in comb]), method))

        return np.array(list_arrays)

@jit(nopython=True)
def single_point_removal_calc(array, threshold ):
    """
        Given a 2D array, uses a square of 3x3 and makes the pixel on the centre 0 if the mean
        value of the neighbouring pixels is smaller than a value (threshold, which is a percentage
        of the value in the centre of the pixel).


    Parameters
    ----------
    array: `np.ndarray`
        2D array.

    Returns
    -------
    positive index: `np.ndarray`
       array with positions where array should be zero.
    """

    positive_indexes = np.zeros((array.shape[0] * array.shape[1], 2), np.int32)
    i = 0

    for row in range(1, array.shape[0]-1):
        for col in range(1, array.shape[0]-1):
            subarray = array[row-1:row+1,col-1:col+1]
            den = (np.sum(subarray) - array[row,col])
            add_pos = False

            if den == 0:
                add_pos = True
            elif array[row, col] / den > threshold:
                add_pos = True

            if add_pos:
                positive_indexes[i, 0] = row
                positive_indexes[i, 1] = col
                i += 1

    positive_indexes = positive_indexes[:i,:]

    return positive_indexes


def single_point_removal(array, threshold ):
    points = single_point_removal_calc(array, threshold)

    array[points[:,0], points[:,1]] = 0

    return array



def return_returncoords(array):
    """
    Given an array, returns an array with coordinates, where each coordinate is the midel point
    of the squares within the array.
    """
    nums = np.unique(np.sort(array[array > 0].ravel()))
    coords = np.zeros((len(nums), 3))

    for i in range(len(nums)):
        num = nums[i]
        where_x, where_y = np.where(array == num)
        x = int((np.min(where_x) + np.max(where_x)) / 2)
        y = int((np.min(where_y) + np.max(where_y)) / 2)
        coords[i, :] = [x, y, num]

    return coords