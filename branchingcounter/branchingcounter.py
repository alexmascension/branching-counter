
from os.path import dirname, abspath
import time
import os

import numpy as np
import pandas as pd
from scipy import sparse, ndimage


from skimage import color
from skimage import io
from skimage.morphology import medial_axis, skeletonize, skeletonize_3d, watershed, thin, closing
from skimage.util import invert
from skimage.restoration import (denoise_tv_chambolle, denoise_bilateral,
                                 denoise_wavelet, estimate_sigma)
from skimage.filters import roberts, sobel, scharr, prewitt
from skimage.exposure import equalize_hist, equalize_adapthist, histogram
from skimage.measure import label

import matplotlib.pyplot as plt

import itertools as it

from numba import jit
from line_profiler import LineProfiler

import h5py
from functools import wraps



from . import arraytools


class Image():
    def __init__(self, path, savedir, verbose = 0, h5 = None):
        """
        Image object. This is the main class of the script. An Image object will store the image
        array, the processed array, location of branches, any other related image of the analysis.

        Parameters
        ----------
        path: str
            Location of the image.
        savedir: str
            Location where h5 file will be stored.
        verbose: int
            Verbosity level.
            - 0: No text.
            - 1: Warnings.
            - 2: All.
        h5: h5py object
            If not `None`, loads this object file instead of opening the file the traditional way.

        General parameters of all methods:
        update_object: bool
            If `True`, stores the object in the HDF object (overrides it if it exists).
        return_object: bool
            If `True`, returns the method object.
        dict_name: str
            Name of the `Image.h5` dataset to be stored.
        """
        self.path = path
        self.savedir = savedir
        self.verbose, self.myfuncverbose = verbose, verbose
        self.image = None

        if h5 is None:
            self.h5 = h5py.File(savedir, 'w')
            self.load_image()
        else:
            if path == savedir:
                self.h5 = h5py.File(savedir, 'a')
            else:
                self.provh5 = h5py.File(path, 'r')
                self.h5 = h5py.File(savedir, 'w')
                for key in self.provh5.keys():
                    self.h5.create_dataset(key, data = self.provh5[key])
                self.provh5.close()



    # Private methods
    def _select_object(self, object_in = None, object_out = ''):
        """
        Loads an object into the method that is calling it. If there is no object to be loaded,
        then a default object, `object_out` is loaded (based on the logical structure of image
        analysis. As a last resort, image array is returned.

        Parameters
        ----------
        object_in: str, int, `np.ndarray`
            Object to be loaded.
        object_out: str, int, `np.ndarray`
            Object to be loaded if `object_in` is `None`.
        """

        if object_in is not None:
            if isinstance(object_in, str):
                return self.h5[object_in].value.copy()
            else:
                return object_in
        else:
            try:
                if isinstance(object_out, str):
                    return self.h5[object_out].value.copy()
                else:
                    return object_out.copy()
            except:
                return self.h5['image'].value.copy()


    def _process_kwargs(self, **kwargs):
        if 'verbose' in kwargs.keys():
            self.myfuncverbose = kwargs['verbose']
        else:
            self.myfuncverbose = self.verbose



    def _applyfunctionND(self, array, func,  *args, verbose=0):
        if len(array.shape) == 2:
            array = func(array, *args)
        elif len(array.shape) == 3:
            list_arrays = []
            print('HOLIIII')
            for j in range(array.shape[0]):
                if verbose:
                    print('Applying function to layer ', j)
                array_temp = func(array[j], *args)
                list_arrays.append(array_temp)
            array = np.array(list_arrays)

        return array





    def _normalize(self, percentile=90, array=None):
        """
        Normalizes an array to have values in 0-1 interval. values to be set to 1 is selected
        with a percentile. Thus values higher than the percentile will be set to 1 automatically.

        Parameters
        ----------
        array: `np.ndarray`
            Array to be normalized.
        percentile: int
            Percentile of normalization. Selects the non zero values of the array, takes the
            percentile-th value, and all values higher than this one will be set to 1.

        Returns
        -------
        array: `np.ndarray`
            Normalized array.
        """

        array = self._select_object(array, self.h5['image'].value.copy())

        return arraytools.normalize(array=array, percentile=percentile)


    def _update_object(self, object_out, name_dict):
        """
        Adds the object to the HDF object. If the object exists, the object is replaced with the
        current version.
        """
        try:
            self.h5.create_dataset(name_dict, data = object_out)
        except:
            del self.h5[name_dict]
            self.h5.create_dataset(name_dict, data=object_out)


    def _count_elements(self, array):
        """
        Given a binarized array, finds independent positive areas and labels them.

        Parameters
        ----------
        array: `np.ndarray`
            Binarized array

        Returns
        -------
        labels: int
            Number of independent elements within the binarized array.
        """

        array = array.copy()

        drops = ndimage.binary_fill_holes(array > 0.5)
        labels = label(drops).max()

        return labels



    # Public methods - Image related
    def load_image(self):
        try:
            self.h5.create_dataset('image', data= np.array(color.rgb2gray(io.imread(self.path))))
        except IsADirectoryError:
            list_arrays = []
            for ROOT, DIRS, FILES in os.walk(self.path):
                for file in FILES:
                    try:
                        list_arrays.append(np.array(color.rgb2gray(io.imread(ROOT+'/'+file))))
                    except:
                        pass

            self.h5.create_dataset('image', data=np.array(list_arrays))


    def remove_background(self, array = None, threshold=0.05,
                update_object = True, return_object = False, dict_name = 'image', *args, **kwargs):
        """
        Removes background noise, that is, removes values of the array smaller than the threshold

        Parameters
        ----------
        array: `np.ndarray`
            Image array.
        threshold: float
            Values smaller than that will be set to 0.

        Returns
        -------
        array: `np.ndarray`
            Denoised array.
        """

        self._process_kwargs(**kwargs)
        array = self._select_object(array, 'image')

        array[array < threshold] = 0

        if update_object: self._update_object(array, dict_name)
        if return_object: return array


    def normalize(self, array = None,
                update_object = True, return_object = False, dict_name = 'image', *args, **kwargs):
        """
        Returns the image in the range 0 - 1

        Parameters
        ----------
        array: `np.ndarray`
            Image array.


        Returns
        -------
        array: `np.ndarray`
            Denoised array.
        """

        self._process_kwargs(**kwargs)
        array = self._select_object(array, 'image')

        array = arraytools.normalize(array, percentile=100)

        if update_object: self._update_object(array, dict_name)
        if return_object: return array

    def denoise(self, array = None, ntrials_max=2, thres=0.01, weight = 0.05,
                update_object = True, return_object = False, dict_name = 'denoised', *args, **kwargs):

        """
        Applies TV Chambolle denoising algorithm to an array, so as to reduce background noise. The
        algorithm is applied recursively, so that, for each iteration, if the sum of the squared
        difference in all the pixels is greater than a value `thres` the algorithm stops.
        the .

        Parameters
        ----------
        array: `np.ndarray`
            Image array.
        ntrials_max: int
            Maximum number of iterations.
        thres: float
            Maximum difference threshold to stop the algorithm.
        weight: float
            Value that relates to denoisification.


        Returns
        -------
        array: `np.ndarray`
            Denoised array.
        """

        self._process_kwargs(**kwargs)
        array = self._select_object(array, 'image')

        array = self._applyfunctionND(array, arraytools.tv_def, ntrials_max, thres)

        if update_object: self._update_object(array, dict_name)
        if return_object: return array


    def remove_spots(self, array = None,  threshold=0.3,
                update_object = True, return_object = False, dict_name = 'denoised', *args, **kwargs):

        """
            Given a 2D array, uses a square of 3x3 and makes the pixel on the centre 0 if the mean
            value of the neighbouring pixels is smaller than a value (threshold, which is a percentage
            of the value in the centre of the pixel).


        Parameters
        ----------
        array: `np.ndarray`
            2D array.
        thres: threshold of selection of spots.

        Returns
        -------
        array: `np.ndarray`
           Corrected array array.
        """

        self._process_kwargs(**kwargs)
        array = self._select_object(array, 'denoised')

        array = self._applyfunctionND(array, arraytools.single_point_removal, threshold)

        if update_object: self._update_object(array, dict_name)
        if return_object: return array


    def contrast(self, array=None, ntrials_max=100, low_pass=0.10, thres=0.01,
                 update_object = True, return_object = False, dict_name = 'contrasted', *args, **kwargs):
        """
          Applies Adaptive histogram normalization to gain contrast in the image array. The
        algorithm is applied recursively, so that, for each iteration, if the sum of the squared
        difference in all the pixels is greater than a value `thres` the algorithm stops.

          Parameters
          ----------
          array: `np.ndarray`
              Image array.
          ntrials_max: int
              Maximum number of iterations.
          thres: float
              Maximum difference threshold to stop the algorithm.
          low_pass: float
              Values smaller than `low_pass` are changed to 0.



          Returns
          -------
          array_positives: `np.ndarray`
              Array of the same size as the image array with 1 and 0, where 1 mean branching events.
          """

        self._process_kwargs(**kwargs)
        array = self._select_object(array, 'denoised')

        array = self._applyfunctionND(array, arraytools.contrast_eqadapthist,
                                      ntrials_max, thres, low_pass)

        if update_object: self._update_object(array, dict_name)
        if return_object: return array



    def tobw(self, array = None, threshold=0.7, update_object = True, return_object = False,
             dict_name = 'bw', *args, **kwargs):

        """
        Changes an array with continuous numbering to a binarized one with a threshold selection.

          Parameters
          ----------
          array: `np.ndarray`
              Image array.
          threshold: float
              Values higher than `threshold` are 1 and else 0.


          Returns
          -------
          array: `np.ndarray`
              Binarized array.
          """

        self._process_kwargs(**kwargs)
        bw = self._select_object(array, 'contrasted')

        bw[bw < threshold] = 0
        bw[bw >= threshold] = 1

        if update_object: self._update_object(bw, dict_name)
        if return_object: return bw


    def get_skeleton(self, array = None, method = '2D', update_object = True, return_object = False,
                        dict_name = 'skeleton', *args, **kwargs):

        """
         Applies skeletonization to binarized array.

          Parameters
          ----------
          array: `np.ndarray`
              Binarized array.
          method: str
              `['2D', '3D']`: If `2D`, obtains the skeleton of a 2D array or a stack of 2D arrays.
              This method returns, for each array , the skeleton with the distance value explained
              in the return. If `3D`, returns the skeletonize_3D object (2D or 3D) array, which
              is only in binary form.

          Returns
          -------
          skeleton_array: `np.ndarray`
              Skeleton array.  Non-zero pixels refer to the relative distance between the
              skeleton center and the nearest border.
          """

        self._process_kwargs(**kwargs)
        array = self._select_object(array, 'bw')

        if method.upper() == '2D':
            distance_on_skel = self._applyfunctionND(array, arraytools.get_skeleton)
        elif method.upper() == '3D':
            distance_on_skel = skeletonize_3d(array>0)

        if update_object: self._update_object(distance_on_skel, dict_name)
        if return_object: return distance_on_skel


    def prune_branches(self, array = None, percentile=10, how='top', update_object = True,
                        return_object = False, dict_name = 'skeleton', *args, **kwargs):

        """
        Removes pixels with smaller or higher values in an skeletonized array. In this way low
        quality branches should be removed.

          Parameters
          ----------
          array: `np.ndarray`
              Skeletonized array.
          percetile: int
              Percentile of non-zero values to apply the pruning threshold.
          how: str
              `top` removes pixels with higher value than threshold, and `bottom` removes pixels
              with lower values than threshold.


          Returns
          -------
          array: `np.ndarray`
              Pruned skeletonized array.
          """
        self._process_kwargs(**kwargs)
        array = self._select_object(array, 'skeleton')

        list_nums = np.sort(array.copy().ravel())
        list_nums = list_nums[list_nums > 0]
        threshold = np.percentile(list_nums, percentile)

        if how.upper == 'TOP':
            array[array > threshold] = 0
        elif how.upper == 'BOTTOM':
            array[array < threshold] = 0

        if update_object: self._update_object(array, dict_name)
        if return_object: return array


    def mark_branchings(self, array = None, w = 5, k = None, update_object = True,
                        return_object = False, dict_name = 'branching points',
                        *args, **kwargs):

        """
        Using an sliding window finds branching events in the image. For each window, independent
        areas are obtained from non skeletonized pixels, and then a vector of the areas in the
        border pixels is obtained. If there combinations of areas is not palindromic, then
        there is a branching or crossing event.



        Parameters
        ----------
        array: `np.ndarray`
            Skeletonized array.
        w: int
            Window of analysis. For each iteration a w x w array will be analyzed to see if there
            is a branching event there.
        k: int
            Marking width. If there is a branching event, a k x k ones array will be printed within
            the array of branchings to mark where the branching event within the image occured.

        Returns
        -------
        array: `np.ndarray`
              1-0 array where 1s are the centers of the windows where a branching phenomenon occured,
              with k extension.
        """

        if k is None: k = int(1.2*w)

        self._process_kwargs(**kwargs)
        array = self._select_object(array, 'skeleton')


        assert w % 2 == 1

        array_branching_points = self._applyfunctionND(array, arraytools.get_branching_points,
                                      w, k, self.myfuncverbose)


        if update_object: self._update_object(array_branching_points, dict_name)
        if return_object: return array_branching_points



    def count_branchings(self, array = None, update_object = True,
                         return_object = False, dict_name = 'branching point count', *args, **kwargs):

        """
        Counts areas with 1s in a 0-1 image.

        Parameters
        ----------
        array: `np.ndarray`
            1-0 array with branching events.


        Returns
        -------
        n_branchings: int
              Number of branching events.
        """
        self._process_kwargs(**kwargs)
        array = self._select_object(array, 'branching points')

        branching_count = self._count_elements(array)

        if update_object: self._update_object(branching_count, dict_name)
        if return_object: return branching_count


    def colorize_binarized_elements(self, array=None, update_object=True,
                          return_object = False, dict_name='colorized elements', **kwargs):
        """
        Given a binarized array, finds independent positive areas and labels them with different
        numbers.

        Parameters
        ----------
        array: `np.ndarray`
            Binarized array

        Returns
        -------
        labels: `np.ndarray`
            Number of independent elements within the binarized array.
        """

        self._process_kwargs(**kwargs)
        array = self._select_object(array, 'branching points')

        drops = ndimage.binary_fill_holes(array > 0.5)
        labels = label(drops)

        if update_object: self._update_object(labels, dict_name)
        if return_object: return labels


    def combine_slides(self, array, n_slides = 3, sliding = True, method = 'mean',
                       update_object=True, return_object=False, dict_name='combined image',
                       **kwargs):
        """
       Given a 3D array, creates another array with stacked layers.

       Parameters
       ----------
       array: `np.ndarray`
           3D array of signal.
       n_slides: int
           Number of slides (2D arrays) that will be considered at each combination iteration.
           If 0, combined all the layers into a 2D array.
       sliding: bool
           If `True`, performs the combination using a aliding window of one; e.g. if there are
           10 slides and `n_slides=3`, then [0,1,2] are stacked together, then [1,2,3], and so on
           until [8,9,10]. If `False`, then [0,1,2], [3,4,5], [6,7,8], [9] are combined.
       method: str
           [`mean`, `sum`] Averages the signal across slides or sums it.

       Returns
       -------
       labels: `np.ndarray`
           Stacked array.
       """

        self._process_kwargs(**kwargs)
        array = self._select_object(array, 'image')

        if len(array.shape) == 2:
            return None

        arraystack = arraytools.get_stack(array, n_slides, sliding, method)

        if update_object: self._update_object(arraystack, dict_name)
        if return_object: return arraystack









def read_image(path, savedir=None):
    return Image(path, savedir, h5 = 'r')

def close_object(image):
    image.h5.close()


def save_imagestack(path, arr, arr2=None, alpha = 0.6, textsize = 12):
    """
    Given an array (2D or 3D), saves the images of those array. Also accepts a second array from
    `colorize_binarized_elements` to draw squares and paint them. Also, it adds the numbers
    corresponding to the cluster.

    Parameters
    ----------
    path: str
        Directory where images will be saved
    arr: np.ndarray
        Array with signals
    arr2: np.ndarray
        Array with clusters
    alpha: float
        Transparency value
    textsize: int
        Size of the text

    Returns
    -------
    Images.
    """
    if not os.path.exists(path): os.makedirs(path)
    for z_i in range(len(arr)):
        fig = plt.figure()
        ax_neu = fig.add_subplot(111)

        ax_neu.imshow(arr[z_i, :, :], cmap="gray")

        if arr2 is None:
            arr2 = np.zeros((arr.shape[0], arr.shape[1], arr.shape[2]))

        arr2 = np.ma.masked_where(arr2 < 0.5, arr2)
        ax_neu.imshow(arr2[z_i, :, :], alpha=alpha, cmap="prism")

        coords = arraytools.return_returncoords(arr2[z_i, :, :])

        for c in range(len(coords)):
            ax_neu.text(x=coords[c, 1], y=coords[c, 0], s=int(coords[c, 2]), color='white',
                        size = textsize, alpha = alpha)

        ax_neu.set_xticks([])
        ax_neu.set_yticks([])

        plt.savefig(path + '/%s.png' % z_i, dpi = 400)



