from mpl_toolkits.mplot3d import Axes3D
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d.art3d import Poly3DCollection
import branchingcounter as bc
import plotly.tools as tls
from plotly.offline import download_plotlyjs, init_notebook_mode, plot, iplot
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.widgets import Slider, Button, RadioButtons
from ipywidgets import interact, widgets
from skimage.morphology import disk
import warnings




def plotSlider(array = None, branchings = None, with_branchings=False):
    """
   Given a 3D array, creates a matplotlib figure with a slider through the different slides
   of the array. It also allows integration with an array of the position of the branchings.

   Parameters
   ----------
   array: `np.ndarray`
       3D array of signal.
   branchings: `np.ndarray`
       3D array with branching voxels.
   with_branchings: bool
        If `True`, implements the `branchings` array into the image.

   Returns
   -------
   labels: `np.ndarray`
       Stacked array.
   """
    def f(Layer):
        with warnings.catch_warnings():
            warnings.simplefilter("ignore")

        ax_neu.imshow(array[int(Layer), :, :], cmap="gray")
        if (with_branchings) & (branchings is not None):
            ax_neu.imshow(branchings[int(Layer), :, :], cmap="prism")

        fig.canvas.draw()


    fig = plt.figure()
    ax_neu = fig.add_subplot(111)
    ax_neu.set_xticks([])
    ax_neu.set_yticks([])


    ax_neu.imshow(array[0, :, :], cmap="gray")
    if (with_branchings) & (branchings is not None):
        ax_neu.imshow(branchings[0, :, :], cmap="prism")

    interact(f, Layer=widgets.IntSlider(min=0, max=len(array), step=1, value=0))




def save_imagestack(path, arr, arr2=None, alpha = 0.6, textsize = 12):
    """
    Given an array (2D or 3D), saves the images of those array. Also accepts a second array from
    `colorize_binarized_elements` to draw squares and paint them. Also, it adds the numbers
    corresponding to the cluster.

    Parameters
    ----------
    path: str
        Directory where images will be saved
    arr: np.ndarray
        Array with signals
    arr2: np.ndarray
        Array with clusters
    alpha: float
        Transparency value
    textsize: int
        Size of the text

    Returns
    -------
    Images.
    """
    if not os.path.exists(path): os.makedirs(path)
    for z_i in range(len(arr)):
        fig = plt.figure()
        ax_neu = fig.add_subplot(111)

        ax_neu.imshow(arr[z_i, :, :], cmap="gray")

        if arr2 is None:
            arr2 = np.zeros((arr.shape[0], arr.shape[1], arr.shape[2]))

        arr2 = np.ma.masked_where(arr2 < 0.5, arr2)
        ax_neu.imshow(arr2[z_i, :, :], alpha=alpha, cmap="prism")

        coords = arraytools.return_returncoords(arr2[z_i, :, :])

        for c in range(len(coords)):
            ax_neu.text(x=coords[c, 1], y=coords[c, 0], s=int(coords[c, 2]), color='white',
                        size = textsize, alpha = alpha)

        ax_neu.set_xticks([])
        ax_neu.set_yticks([])

        plt.savefig(path + '/%s.png' % z_i, dpi = 400)