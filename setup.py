import setuptools

with open("README.md", 'r') as f:
    long_description = f.read()

setuptools.setup(
   name='branchingcounter',
   version='1.0',
   description='branchingcounter - a branching event counter',
   long_description=long_description,
   license="LICENSE",
   author='Alex M. Ascension',
   author_email='alexmascension@gmail.com',
   url="https://gitlab.com/alexmascension/branchingcounter",
   install_requires=['numpy>=1.14.3', 'pandas>=0.23.4', 'scikit-image>=0.14.2', 'matplotlib>=3.0.2',
                     'numba>=0.41', 'line_profiler>=2.1.2'],
   include_package_data=True,
   packages=setuptools.find_packages(),
   classifiers=[
      "Programming Language :: Python :: 3",
      "License :: OSI Approved :: GNU Library or Lesser General Public License (LGPL)",
      "Operating System :: OS Independent",
   ],
)

